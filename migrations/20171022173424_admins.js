exports.up = knex => {
    return  knex.schema.createTable('admins', table => {
        table.binary('id', 36).primary();
		table.binary('account_id', 36);
        table.string('nickname', 32);
        table.string('authid', 22);
        table.string('password', 40);
        table.integer('flags');
		table.timestamps(true, true);
    });
};

exports.down = knex => {
    return knex.schema.dropTable('admins');
};
