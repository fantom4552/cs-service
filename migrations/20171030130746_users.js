exports.up = knex => {
	return  knex.schema.createTable('users', table => {
		table.binary('id', 36).primary();
		table.string('email', 255);
		table.string('password', 64);
		table.timestamps(true, true);
	});
};

exports.down = knex => {
	return knex.schema.dropTable('users');
};
