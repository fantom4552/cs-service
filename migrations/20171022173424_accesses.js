exports.up = knex => {
    return  knex.schema.createTable('accesses', table => {
        table.binary('id', 36).primary();
		table.binary('server_id', 36);
        table.string('title', 32);
        table.integer('access');
		table.timestamps(true, true);
    });
};

exports.down = knex => {
    return knex.schema.dropTable('accesses');
};
