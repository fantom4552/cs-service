exports.up = knex => {
    return  knex.schema.createTable('servers_admins', table => {
        table.binary('server_id', 36);
        table.binary('admin_id', 36);
        table.binary('access_id', 36);
        table.date('expired');
        table.timestamps(true, true);
        table.primary(['server_id', 'admin_id']);
    });
};

exports.down = knex => {
    return knex.schema.dropTable('servers_admins');
};
