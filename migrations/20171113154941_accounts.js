exports.up = knex => {
	return  knex.schema.createTable('accounts', table => {
		table.binary('id', 36).primary();
		table.binary('owner', 36);
		table.timestamps(true, true);
	});
};

exports.down = knex => {
	return knex.schema.dropTable('accounts');
};
