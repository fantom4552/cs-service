exports.up = knex => {
    return  knex.schema.createTable('servers', table => {
        table.binary('id', 36).primary();
        table.binary('account_id', 36);
        table.string('title', 32);
        table.string('address', 22);
		table.timestamps(true, true);
    });
};

exports.down = knex => {
    return knex.schema.dropTable('servers');
};
