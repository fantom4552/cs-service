# Counter-Strike Service

### Description
**Counter-Strike Service** is the system written on Node JS which have more capabilities than other systems

### Requirements
- Node JS >= 8
- NPM >= 5
- MySQL >= 5.5

### Installation
1. Clone repository
2. Run `npm install`
3. Copy `.env.example` to `.env`
4. Copy `config/default.json` to `{ENV}.json` and make you changes
5. Run `npm run migrate:up`
5. Run `npm run start`
