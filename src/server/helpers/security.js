const util = require('util');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');
const jwtSign = util.promisify(jwt.sign);
const jwtVerify = util.promisify(jwt.verify);
const bcryptGenerateSalt = util.promisify(bcrypt.genSalt);
const bcryptHash = util.promisify(bcrypt.hash);
const bcryptCompare = util.promisify(bcrypt.compare);

const passwordHash = async (password) => {
	return bcryptGenerateSalt(5)
		.then(salt => bcryptHash(password, salt, null));
};

const passwordVerify = async (password, hash) => {
	return bcryptCompare(password, hash);
};

const tokenGenerate = async (data, secret) => {
	return jwtSign(data, secret, {
		algorithm: 'HS256',
		expiresIn: '1h'
	});
};

const tokenVerify = async (token, secret) => {
	return jwtVerify(token, secret, {
		algorithms: ['HS256'],
		ignoreExpiration: (process.env.NODE_ENV == 'dev')
	});
};

module.exports = {
	tokenGenerate,
	tokenVerify,
	passwordHash,
	passwordVerify
};
