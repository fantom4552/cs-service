const config = require('config');
const Knex = require('knex');

const db = Knex({
    client: 'mysql2',
	// debug: true,
    connection: {
        host : config.get('DataBase.host'),
        port: config.get('DataBase.port'),
        user : config.get('DataBase.user'),
        password : config.get('DataBase.pass'),
        database : config.get('DataBase.dbname'),
        timezone : config.get('DataBase.timezone'),
        charset : config.get('DataBase.charset'),
        dateStrings: false
    },
    pool: config.get('DataBase.pool'),
    useNullAsDefault: true
});

const dbOptions = { dialect: 'mysql' };

const dbCall = (sql, ctx) => {
	return db.raw(sql, ctx.args || {}).then(([data]) => {
		return data;
	});
};

const dbArgs = (args, ctx) => {
	if (!ctx.args) {
		ctx.args = {};
	}

	Object.assign(ctx.args, args);
}

module.exports = {
    db,
	dbOptions,
	dbCall,
	dbArgs
};
