const path = require('path');
const webpack = require('webpack');
const MemoryFileSystem = require('memory-fs');
const mime = require('mime');

module.exports = function (opt) {
	opt = Object.assign({
		config: './webpack.config.js',
		watchOptions: {
			aggregateTimeout: 300
		},
		log: {
			level: 'info'
		},
		stats: {
			chunks: false,
			colors: true
		},
		defaultPage: 'index.html'
	}, opt);

	const compiler = webpack(
		typeof opt.config === 'string' ? require(path.resolve(process.cwd(), opt.config)) : opt.config
	);

	const outputPath = process.cwd();
	let compiling = true;

	const mfs = new MemoryFileSystem();
	compiler.outputFileSystem = mfs;

	compiler.plugin('invalid', () => {
		compiling = true;
		console.info('Assets are now invalid.');
	});
	compiler.plugin('compile', () => {
		compiling = true;
		console.info('Compiling assets...');
	});

	compiler.watch(opt.watchOptions, (err, stats) => {
		compiling = false;
		if (err) {
			console.error(err.message || err, err.message && err);
			throw err;
		}
		const jsStats = stats.toJson('minimal');
		if (jsStats.errors && jsStats.errors.length > 0) {
			console.error('error', 'Error occurred during compiling.', jsStats.errors);
		} else if (jsStats.warnings && jsStats.warnings.length > 0) {
			console.warn('Warnings recorded during compiling.', jsStats.warnings);
		} else {
			console.info('Rebuild completed.');
		}
	});

	return async (ctx, next) => {
		if (ctx.method !== 'GET') {
			await next();
		} else if (compiling) {
			await next();
		} else {
			let requestFile = path.join(outputPath, ctx.path);

			try {
				let stat = mfs.statSync(requestFile);
				if (stat.isDirectory()) {
					requestFile = path.join(requestFile, opt.defaultPage);
					stat = mfs.statSync(requestFile);
				}

				if (!stat.isFile()) {
					await next();
				} else {
					const content = mfs.readFileSync(requestFile);
					ctx.set("Access-Control-Allow-Origin", "*"); // To support XHR, etc.

					ctx.set("Content-Type", mime.lookup(requestFile));
					ctx.set("Content-Length", content.length);

					ctx.body = content;
				}
			} catch (e) {
				await next();
			}
		}
	};
};
