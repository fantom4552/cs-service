const { tokenVerify } = require('../helpers/security');

const verifyToken = async (ctx, next) => {
	let token = ctx.cookies.get('jwt');
	if (!token) {
		token = ctx.request.body.token;
	}
	if (!token) {
		ctx.throw(401, 'Unauthorized');
	} else {
		const data = await tokenVerify(token, process.env.JWT_SECRET);
		if (!data) {
			ctx.throw(401, 'Unauthorized');
		} else {
			ctx.state.user = Buffer.from(data.user).toString();
			ctx.state.account = Buffer.from(data.account).toString();
			await next();
		}
	}
};

module.exports = {
	verifyToken
};
