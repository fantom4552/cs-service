const Model = require('./model');

class ServerAdminModel extends Model {
	static getTableName() {
		return 'servers_admins';
	}

	static getFields() {
		return ['server_id', 'admin_id', 'access_id', 'expired'];
	}

	static async create(args) {
		const attrs = { ...args };
		await this.dbInsert(attrs);
		return await this.dbSelect(this.getPrimary(args.server_id, args.admin_id));
	}

	static async update(server_id, admin_id, args) {
		const primary = this.getPrimary(server_id, admin_id);
		await this.dbUpdate(args, primary);
		return await this.dbSelect(primary);
	}

	static async delete(server_id, admin_id) {
		const primary = this.getPrimary(server_id, admin_id);
		await this.dbRemove(primary);
		return primary;
	}

	static getPrimary(server_id, admin_id) {
		return { server_id, admin_id };
	}
}

module.exports = ServerAdminModel;
