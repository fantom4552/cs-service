const uuidv4 = require('uuid/v4');

const db = require('../helpers/db').db;

class Model {
	static async dbSelect(params, fields = undefined) {
		const [data] = await db.select(fields || this.getFields()).from(this.getTableName()).where(params);
		return data;
	}

	static async dbInsert(attrs) {
		return db.insert(attrs).into(this.getTableName());
	}

	static async dbUpdate(attrs, primary) {
		return db.update(attrs).into(this.getTableName()).where(primary);
	}

	static async dbRemove(primary) {
	    return db
	        .from(this.getTableName())
	        .where(primary)
	        .del();

	}

	static generateId() {
		return uuidv4();
	}
}


module.exports = Model;
