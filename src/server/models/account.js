const Model = require('./model');
const validator = require('../validations');

class AccountModel extends Model {
	static getTableName() {
		return 'accounts';
	}

	static getFields() {
		return ['id', 'owner'];
	}

	static async create(args) {
		const id = this.generateId();
		const attrs = {
			...args,
			id
		};

		// if (validator.validate('user', attrs) !== undefined) {
		// 	throw new Error('Error');
		// }

		const account = await this.dbSelect({ owner: attrs.owner });
		if (account) {
			throw new Error('Account already exists');
		}

		await this.dbInsert(attrs);
		return await this.dbSelect(this.getPrimary(id));
	}

	static async getAccountByOwner(owner) {
		const account = await this.dbSelect({ owner });
		if (!account) {
			throw new Error('Account not found');
		}
		return account;
	}

	static getPrimary(id) {
		return { id };
	}
}

module.exports = AccountModel;
