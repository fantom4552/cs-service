const Model = require('./model');
const validator = require('../validations');
const { passwordHash, passwordVerify } = require('../helpers/security');

class UserModel extends Model {
	static getTableName() {
		return 'users';
	}

	static getFields() {
		return ['id', 'email'];
	}

	static async create(args) {
		const id = this.generateId();
		const password = await passwordHash(args.password);
		const attrs = {
			...args,
			id,
			password,
		};

		if (validator.validate('user', attrs) !== undefined) {
			throw new Error('Error');
		}

		const user = await this.dbSelect({ email: attrs.email });
		if (user) {
			throw new Error('User already exists');
		}

		await this.dbInsert(attrs);
		return await this.dbSelect(this.getPrimary(id));
	}

	static async getUserByEmail(email) {
		const user = await this.dbSelect({ email }, ['id', 'email', 'password']);
		if (!user) {
			throw new Error('User not found');
		}
		return user;
	}

	static async verifyUserPassword(password, hash) {
		if (!await passwordVerify(password, hash)) {
			throw new Error('Bad password');
		}
	}

	static getPrimary(id) {
		return { id };
	}
}

module.exports = UserModel;
