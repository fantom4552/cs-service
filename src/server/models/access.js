const Model = require('./model');
const validator = require('../validations');

class AccessModel extends Model {
	static getTableName() {
		return 'accesses';
	}

	static getFields() {
		return ['id', 'server_id', 'title', 'access'];
	}

	static async create(args) {
		const id = this.generateId();
		const attrs = {
			...args,
			id,
		};

		// if (validator.validate('server', attrs) !== undefined) {
		// 	throw new Error('Error');
		// }

		await this.dbInsert(attrs);
		return await this.dbSelect(this.getPrimary(id));
	}

	static async update(id, args) {
		// if (validator.validate('server', {id, ...args}) !== undefined) {
		// 	throw new Error('Error');
		// }

		const primary = this.getPrimary(id);
		await this.dbUpdate(args, primary);
		return await this.dbSelect(primary);
	}

	static async delete(id) {

		const primary = this.getPrimary(id);
		await this.dbRemove(primary);
		return primary;
	}

	static getPrimary(id) {
		return { id };
	}
}

module.exports = AccessModel;
