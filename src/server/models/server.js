const Model = require('./model');
const validator = require('../validations');

class ServerModel extends Model {
	static getTableName() {
		return 'servers';
	}

	static getFields() {
		return ['id', 'address', 'title'];
	}

	static async create(accountId, args) {
		const id = this.generateId();
		const attrs = {
			...args,
			id: id,
			account_id: accountId,
		};
		if (validator.validate('server', attrs) !== undefined) {
			throw new Error('Error');
		}

		const server = await this.dbSelect({ address: attrs.address });
		if (server) {
			throw new Error('Server already exists');
		}

		await this.dbInsert(attrs);
		return await this.dbSelect(this.getPrimary(id));
	}

	static async update(id, args) {
		if (validator.validate('server', {id, ...args}) !== undefined) {
			throw new Error('Error');
		}

		const primary = this.getPrimary(id);
		await this.dbUpdate(args, primary);
		return await this.dbSelect(primary);
	}

	static async delete(id) {

		const primary = this.getPrimary(id);
		await this.dbRemove(primary);
		return primary;
	}

	static getPrimary(id) {
		return { id };
	}
}

module.exports = ServerModel;
