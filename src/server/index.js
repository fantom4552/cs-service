require('dotenv').config();
const config = require('config');

const pino = require('pino');

const app = require('./app');

const pretty = pino.pretty();
pretty.pipe(process.stdout);
const logger = pino({
	name: 'app',
	safe: true
}, pretty);

app.listen(config.get('App.port'), () => {
	logger.info(`App listening on port ${config.get('App.port')}!`);
});
