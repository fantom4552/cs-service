module.exports = {
	properties: {
		id: {
			type: 'string',
			pattern: '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$'
		},
		email: {
			type: 'string',
			format: 'email',
			minLength: 1
		},
		password: {
			type: 'string',
            minLength: 1
		}
	},
	required: ['email', 'password'],
};
