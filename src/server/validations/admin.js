module.exports = {
	properties: {
		id: {
			type: 'string',
			pattern: '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$'
		},
		nickname: {
			type: 'string',
			maxLength: 32
		},
		authid: {
			type: 'string',
			pattern: '^(?:STEAM|VALVE)_\\d:\\d:\\d{1,11}$'
		}
	}
};
