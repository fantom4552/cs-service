const djv = require('djv');
const validator = new djv();

const serverValidations = require('./server');
const adminValidations = require('./admin');
const userValidations = require('./user');

validator.addSchema('server', serverValidations);
validator.addSchema('admin', adminValidations);
validator.addSchema('user', userValidations);

module.exports = validator;
