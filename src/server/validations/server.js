module.exports = {
	properties: {
		id: {
			type: 'string',
			pattern: '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$'
		},
		address: {
			type: 'string',
			pattern: '^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d{3,5}$'
		},
		title: {
			type: 'string',
			maxLength: 32
		}
	}
};
