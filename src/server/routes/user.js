const { verifyToken } = require('../middlewares/security');
const { tokenGenerate } = require('../helpers/security');
const UserModel = require('../models/user');
const AccountModel = require('../models/account');

const registerAction = async (ctx) => {
	// TODO: Make in transaction
	const user = await UserModel.create(ctx.request.body);
	const account = await AccountModel.create({
		owner: user.id
	});

	ctx.body = {
		id: user.id.toString(),
		account: account.id.toString(),
	};
};

const loginAction = async (ctx) => {
	const email = ctx.request.body.email;
	const password = ctx.request.body.password;

	const user = await UserModel.getUserByEmail(email);
	await UserModel.verifyUserPassword(password, user.password);
	const account = await AccountModel.getAccountByOwner(user.id);

	const token = await tokenGenerate({
		user: user.id,
		account: account.id
	}, process.env.JWT_SECRET);

	ctx.cookies.set('jwt', token);
	ctx.body = {
		token
	};
};

const accountAction = async (ctx) => {
	const user = ctx.state.user.toString();
	const account = ctx.state.account.toString();
	ctx.body = {
		user,
		account
	};
};

const logoutAction = async (ctx) => {
	ctx.cookies.set('jwt', '');
	ctx.body = {
		success: true
	};
};

module.exports = (router) => {
	router.post('/register', registerAction);
	router.post('/login', loginAction);
	router.post('/account', verifyToken, accountAction);
	router.get('/logout', logoutAction);
};
