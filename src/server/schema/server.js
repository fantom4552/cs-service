const {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
    GraphQLNonNull
} = require('graphql');

const joinMonster = require('join-monster').default;

const {
	dbOptions,
	dbCall,
	dbArgs
} = require('../helpers/db');

const ServerModel = require('../models/server');

let ServerAdmin = null;
let Access = null;

const Server = new GraphQLObjectType({
	name: 'Server',
	sqlTable: ServerModel.getTableName(),
	uniqueKey: 'id',
	fields: () => {
		if (Access === null) {
			Access = require('./access').Access;
		}
		if (ServerAdmin === null) {
			ServerAdmin = require('./server_admin').ServerAdmin;
		}
		return {
			id: {
				type: GraphQLString
			},
			title: {
				type: GraphQLString
			},
			address: {
				type: GraphQLString
			},
			server_admins: {
				type: new GraphQLList(ServerAdmin),
				sqlBatch: {
					thisKey: 'server_id',
					parentKey: 'id'
				},
			},
			accesses: {
				type: new GraphQLList(Access),
				sqlBatch: {
					thisKey: 'server_id',
					parentKey: 'id'
				},
			}
		};
	}
});

const servers = {
	type: new GraphQLList(Server),
	where: (table, args, ctx) => {
		dbArgs({
			accountId: ctx.state.account
		}, ctx);
		return `${table}.account_id = :accountId`
	},
	resolve: (root, args, context, resolveInfo) => {
		return joinMonster(resolveInfo, context, sql => dbCall(sql, context), dbOptions);
	}
};

const server = {
	type: Server,
	args: {
		id: {
			type: GraphQLString
		}
	},
	where: (table, args, ctx) => {
		dbArgs({
			accountId: ctx.state.account,
			serverId: args.id
		}, ctx);
		return `${table}.account_id = :accountId AND ${table}.id = :serverId`
	},
	resolve: (root, args, context, resolveInfo) => {
		return joinMonster(resolveInfo, context, sql => dbCall(sql, context), dbOptions);
	}
};

const createServer = {
	type: Server,
    args: {
        address: {
            type: new GraphQLNonNull(GraphQLString)
        },
        title: {
			type: GraphQLString
        }
    },
    resolve: (root, args, ctx) => ServerModel.create(root.getAccountId(ctx), args)
};

const updateServer = {
	type: Server,
	args: {
		id: {
			type: new GraphQLNonNull(GraphQLString)
		},
		address: {
			type: GraphQLString
		},
		title: {
			type: GraphQLString
		}
	},
	resolve: (_, args) => {
		const { id, ...attrs} = args;
		return ServerModel.update(id, attrs);
	}
};

const deleteServer = {
	type: Server,
	args: {
		id: {
			type: new GraphQLNonNull(GraphQLString)
		}
	},
	resolve: (root, args) => ServerModel.delete(args.id)
};

module.exports = {
    Server,
    servers,
	server,
	createServer,
	updateServer,
	deleteServer,
};
