const {
	GraphQLObjectType,
	GraphQLList,
	GraphQLString,
	GraphQLInt,
	GraphQLNonNull
} = require('graphql');

const ServerAdminModel = require('../models/server_admin');

const Server = require('./server').Server;
const Admin = require('./admin').Admin;
const Access = require('./access').Access;

const ServerAdmin = new GraphQLObjectType({
	name: 'ServerAdmin',
	sqlTable: ServerAdminModel.getTableName(),
	uniqueKey: ['server_id', 'admin_id'],
	fields: () => ({
		expired: {
			type: GraphQLInt
		},
		server: {
			type: Server,
			sqlBatch: {
				thisKey: 'id',
				parentKey: 'server_id'
			},
		},
		admin: {
			type: Admin,
			sqlBatch: {
				thisKey: 'id',
				parentKey: 'admin_id'
			},
		},
		access: {
			type: Access,
			sqlBatch: {
				thisKey: 'id',
				parentKey: 'access_id'
			},
		}
	})
});

const addAdminToServer = {
	type: ServerAdmin,
	args: {
		server_id: {
			type: new GraphQLNonNull(GraphQLString)
		},
		admin_id: {
			type: new GraphQLNonNull(GraphQLString)
		},
		access_id: {
			type: GraphQLString
		},
		expired: {
			type: GraphQLInt
		}
	},
	resolve: (_, args) => ServerAdminModel.create(args)
};

const updateAdminForServer = {
	type: ServerAdmin,
	args: {
		server_id: {
			type: new GraphQLNonNull(GraphQLString)
		},
		admin_id: {
			type: new GraphQLNonNull(GraphQLString)
		},
		access_id: {
			type: GraphQLString
		},
		expired: {
			type: GraphQLInt
		}
	},
	resolve: (_, args) => {
		const { server_id, admin_id, ...attrs} = args;
		return ServerAdminModel.update(server_id, admin_id, attrs);
	}
};

const removeAdminFromServer = {
	type: ServerAdmin,
	args: {
		server_id: {
			type: new GraphQLNonNull(GraphQLString)
		},
		admin_id: {
			type: new GraphQLNonNull(GraphQLString)
		},
	},
	resolve: (_, args) => ServerAdminModel.delete(args.server_id, args.admin_id)
};

module.exports = {
	ServerAdmin,
	addAdminToServer,
	updateAdminForServer,
	removeAdminFromServer
};
