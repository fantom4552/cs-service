const {
	GraphQLObjectType,
	GraphQLSchema
} = require('graphql');

const {
	addAdminToServer,
	updateAdminForServer,
	removeAdminFromServer
} = require('./server_admin');

const {
	servers,
	server,
	createServer,
	updateServer,
	deleteServer
} = require('./server');

const {
	admins,
	admin,
	createAdmin,
	updateAdmin,
	deleteAdmin
} = require('./admin');

const {
	accesses,
	access,
	createAccess,
	updateAccess,
	deleteAccess,
} = require('./access');

const QueryRoot = new GraphQLObjectType({
	name: 'Query',
	fields: {
		servers,
		server,
		admins,
		admin,
		accesses,
		access,
	}
});

const MutationRoot = new GraphQLObjectType({
	name: 'Mutation',
	fields: {
		createServer,
		updateServer,
		deleteServer,
		createAdmin,
		updateAdmin,
		deleteAdmin,
		createAccess,
		updateAccess,
		deleteAccess,
		addAdminToServer,
		updateAdminForServer,
		removeAdminFromServer,
	}
});

const schema = new GraphQLSchema({
	query: QueryRoot,
	mutation: MutationRoot,
});

module.exports = schema;
