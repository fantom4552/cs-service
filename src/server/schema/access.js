const {
    GraphQLObjectType,
    GraphQLList,
    GraphQLString,
	GraphQLInt,
    GraphQLNonNull
} = require('graphql');

const joinMonster = require('join-monster').default;

const {
	dbOptions,
	dbCall,
	dbArgs
} = require('../helpers/db');

const AccessModel = require('../models/access');

let Server = null;

const Access = new GraphQLObjectType({
	name: 'Access',
	sqlTable: AccessModel.getTableName(),
	uniqueKey: 'id',
	fields: () => {
		if (Server === null) {
			Server = require('./server').Server;
		}
		return {
			id: {
				type: GraphQLString
			},
			title: {
				type: GraphQLString
			},
			access: {
				type: GraphQLInt
			},
			server: {
				type: Server,
				sqlBatch: {
					thisKey: 'id',
					parentKey: 'server_id'
				},
			}
		};
	}
});

const accesses = {
	type: new GraphQLList(Access),
	resolve: (root, args, context, resolveInfo) => {
		return joinMonster(resolveInfo, context, sql => dbCall(sql, context), dbOptions);
	}
};

const access = {
	type: Access,
	args: {
		id: {
			type: GraphQLString
		}
	},
	where: (table, args, ctx) => {
		dbArgs({
			accessId: args.id
		}, ctx);
		return `${table}.id = :accessId`
	},
	resolve: (root, args, context, resolveInfo) => {
		return joinMonster(resolveInfo, context, sql => dbCall(sql, context), dbOptions);
	}
};

const createAccess = {
	type: Access,
    args: {
        server_id: {
            type: new GraphQLNonNull(GraphQLString)
        },
        title: {
            type: new GraphQLNonNull(GraphQLString)
        },
        access: {
			type: new GraphQLNonNull(GraphQLInt)
        }
    },
    resolve: (root, args) => AccessModel.create(args)
};

const updateAccess = {
	type: Access,
	args: {
		id: {
			type: new GraphQLNonNull(GraphQLString)
		},
		title: {
			type: GraphQLString
		},
		access: {
			type: GraphQLInt
		}
	},
	resolve: (root, args) => {
		const { id, ...attrs} = args;
		return AccessModel.update(id, attrs);
	}
};

const deleteAccess = {
	type: Access,
	args: {
		id: {
			type: new GraphQLNonNull(GraphQLString)
		}
	},
	resolve: (root, args) => AccessModel.delete(args.id)
};

module.exports = {
    Access,
    accesses,
	access,
	createAccess,
	updateAccess,
	deleteAccess,
};
