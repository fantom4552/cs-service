const {
	GraphQLObjectType,
	GraphQLList,
	GraphQLString,
	GraphQLNonNull
} = require('graphql');

const joinMonster = require('join-monster').default;

const {
	dbOptions,
	dbCall,
	dbArgs
} = require('../helpers/db');

const AdminModel = require('../models/admin');

let ServerAdmin = null;

const Admin = new GraphQLObjectType({
	name: 'Admin',
	sqlTable: AdminModel.getTableName(),
	uniqueKey: 'id',
	fields: () => {
		if (ServerAdmin === null) {
			ServerAdmin = require('./server_admin').ServerAdmin;
		}
		return {
			id: {
				type: GraphQLString
			},
			nickname: {
				type: GraphQLString
			},
			authid: {
				type: GraphQLString
			},
			password: {
				type: GraphQLString
			},
			admin_servers: {
				type: new GraphQLList(ServerAdmin),
				sqlBatch: {
					thisKey: 'admin_id',
					parentKey: 'id'
				},
			}
		};
	}
});

const admins = {
	type: new GraphQLList(Admin),
	where: (table, args, ctx) => {
		dbArgs({
			accountId: ctx.state.account
		}, ctx);
		return `${table}.account_id = :accountId`
	},
	resolve: (_, args, context, resolveInfo) => {
		return joinMonster(resolveInfo, context, sql => dbCall(sql, context), dbOptions);
	}
};

const admin = {
	type: Admin,
	args: {
		id: {
			type: new GraphQLNonNull(GraphQLString)
		}
	},
	where: (table, args, ctx) => {
		dbArgs({
			accountId: ctx.state.account,
			adminId: args.id
		}, ctx);
		return `${table}.account_id = :accountId AND ${table}.id = :adminId`
	},
	resolve: (_, args, context, resolveInfo) => {
		return joinMonster(resolveInfo, context, sql => dbCall(sql, context), dbOptions);
	}
};

const createAdmin = {
	type: Admin,
	description: 'Create admin',
	args: {
		nickname: {
			type: GraphQLString
		},
        authid: {
			type: new GraphQLNonNull(GraphQLString)
		}
	},
	resolve: (root, args, ctx) => AdminModel.create(root.getAccountId(ctx), args)
};

const updateAdmin = {
	type: Admin,
	args: {
		id: {
			type: new GraphQLNonNull(GraphQLString)
		},
		nickname: {
			type: GraphQLString
		},
		authid: {
			type: GraphQLString
		}
	},
	resolve: (_, args) => {
		const { id, ...attrs} = args;
		return AdminModel.update(id, attrs);
	}
};

const deleteAdmin = {
	type: Admin,
	args: {
		id: {
			type: new GraphQLNonNull(GraphQLString)
		}
	},
    resolve: (_, args) => AdminModel.delete(args.id)
};

module.exports = {
	Admin,
	admins,
	admin,
	createAdmin,
	updateAdmin,
	deleteAdmin,
};
