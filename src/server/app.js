const Koa = require('koa');
const KoaRouter = require('koa-router');
const graphqlHTTP = require('koa-graphql');
const depthLimit = require('graphql-depth-limit');
const bodyParser = require('koa-bodyparser');

const { verifyToken } = require('./middlewares/security');
const userRoute = require('./routes/user');

const app = new Koa();

const webpackDevServer = require('./middlewares/webpack-dev');

app.use(webpackDevServer({
	config: './webpack.config.js'
}));

app.use(bodyParser({
	enableTypes: ['json'],
	encode: 'utf-8',
	strict: true,
	onerror: function (err, ctx) {
		ctx.throw(422, 'body parse error');
	}
}));

app.use(async (ctx, next) => {
	try {
		await next();
	} catch (e) {
		ctx.status = e.status || 500;
		ctx.body = {
			error: e.message
		}
	}
});

const router = new KoaRouter();

const fs = require('fs');
const path = require('path');
const indexStream = fs.createReadStream(path.resolve(__dirname, '../../index.html'));

router.get('/', (ctx) => {
	ctx.status = 200;
	ctx.type = 'text/html';
	ctx.body = indexStream;
});

userRoute(router);

router.use('/api', verifyToken);

const root = {
	getAccountId: function (ctx) {
		return ctx.state.account;
	}
};

const schema = require('./schema');

router.all('/api', graphqlHTTP({
    schema: schema,
	rootValue: root,
	formatError: (e) => {
    	return e.message;
	},
	validationRules: [
		depthLimit(3)
	],
    graphiql: true
}));

app.use(router.routes()).use(router.allowedMethods());

module.exports = app;
