const path = require('path');

module.exports = {
	entry: path.join(__dirname, 'src/client/index.jsx'),
	output: {
		path: path.join(__dirname, 'assets'),
		filename: 'bundle.js',
		publicPath: '/assets'
	},
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				loader: 'babel-loader',
				include: path.join(__dirname, 'src/client'),
				exclude: /node_modules/,
				options: {
					presets: ['es2015', 'stage-1'],
					// plugins: [
					// 	'transform-es2015-parameters',
					// 	'transform-es2015-destructuring'
					// ]
				}
			}
		]
	},

};
