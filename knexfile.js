require('dotenv').config();

const config = require('config');

module.exports = {
	client: 'mysql2',
	connection: {
		host : config.get('DataBase.host'),
		port: config.get('DataBase.port'),
		user : config.get('DataBase.user'),
		password : config.get('DataBase.pass'),
		database : config.get('DataBase.dbname'),
		timezone : config.get('DataBase.timezone'),
		charset : config.get('DataBase.charset'),
		dateStrings: false
	},
	pool: config.get('DataBase.pool'),
	useNullAsDefault: true,
	migrations: {
		tableName: 'migrations'
	}
};
